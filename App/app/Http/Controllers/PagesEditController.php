<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PagesEditController extends Controller
{
    public function execute(Page $page, Request $request){
        // эти строчки кода мы можем заменить
        // введением простой зависимости execute(Page $page, ...)
        /*
        $page = Page::find($request->page);
        $old = $page->toArray();
        dd($old);
        */

        if($request->isMethod('delete'))
        {
            $page->delete();

            return redirect('admin')->with('status','Page was deleted');
        }

        if($request->isMethod('post'))
        {
            $input = $request->except('_token');
            $validator = Validator::make($input,[
                'name' => 'required|max:255',
                'alias' => 'required|max:255|unique:pages,alias,'.$input['id'], // имя таблицы, поля, id записи исключенной из проверки
                'text' => 'required'
            ]);
            if ($validator->fails())
            {
                return redirect()->route('pagesEdit',['page' => $input['id']])->withErrors($validator);
            }
            // загружается ли файл на сервер?
            if ($request->hasFile('images'))
            {
                $file = $request->file('images');
                $file->move(public_path().'/assets/img',$file->getClientOriginalName());
                $input['images'] = $file->getClientOriginalName();
            }
            else{
                $input['images'] = $input['old_images'];
            }
            unset($input['old_images']);
            $page->fill($input);
            // SQL UPDATE - обновим состояние модели и редиректим
            if ($page->update()){
                return redirect('admin')->with('status','Страница обновлена');
            }
        }

        if (view()->exists('admin.pages_edit'))
        {
            $data = [
                'title' => 'Редактирование страницы - '.$page->name,
                'data' => $page
            ];
            return view('admin.pages_edit',$data);
        }
        return abort(404);

    }
}