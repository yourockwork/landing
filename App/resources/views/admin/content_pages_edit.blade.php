<div class="wrapper container-fluid">
    {!! Form::open(['url'=>route('pagesEdit',['page'=>$data['id']]), 'class'=>'form-horizontal', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::hidden('id',$data['id']) !!}
        {!! Form::label('name','Page name',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('name',$data['name'],['class'=>'form-control','placeholder'=>'Edit text']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('alias','Page alias',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('alias',$data['alias'],['class'=>'form-control','placeholder'=>'Edit alias']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('text','Text page',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {{-- id нужен для визуального редактора --}}
            {!! Form::textarea('text',$data['text'],['id'=>'text','class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images','Page images',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-offset-2 col-xs-10">
            {!! Html::image('assets/img/'.$data['images'],'',['class'=>'img-responsive']) !!}
            {!! Form::hidden('old_images',$data['images']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images','Page images',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'btn-primary','data-placeholder'=>'File is absent']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Save <i class="fa fa-save"></i>',['class'=>'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>