<!-- Container -->
<div class="container portfolio_title">
    <!-- Title -->
    <div class="section-title">
        <h2>{{$title}}</h2>
    </div>
    <!--/Title -->
</div>
<!-- Container -->

<!-- Portfolio Filters -->
<div class="portfolio">
    <div class="sixteen columns">
        <ul>
            <li>
                <a href="{{route('pages')}}">
                    <h5>All</h5>
                </a>
            </li>
            <li>
                <a href="{{route('portfolio')}}">
                    <h5>Portfolio</h5>
                </a>
            </li>
            <li>
                <a href="{{route('services')}}">
                    <h5>Services</h5>
                </a>
            </li>
            <li>
                <a href="{{route('home')}}">
                    <h5>Login</h5>
                </a>
            </li>
        </ul>
    </div>
</div>
<!--/Portfolio Filters -->