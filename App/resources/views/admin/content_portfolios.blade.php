<div>
    {!! Html::link(route('portfolioAdd'),'Add new portfolio',['alt' => 'add new portfolio']) !!}
    @if($portfolios)
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Name</th>
                    <th>Images</th>
                    <th>Filter</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($portfolios as $k => $portfolio)
                    <tr>
                        <td>{{$portfolio->id}}</td>
                        <td>{!! Html::link(route('portfolioEdit',['page'=>$portfolio->id]),$portfolio->name,['alt' => $portfolio->name]) !!}</td>
                        <td>{{$portfolio->images}}</td>
                        <td>{{$portfolio->filter}}</td>
                        <td>{{$portfolio->created_at}}</td>
                        <td>{{$portfolio->updated_at}}</td>
                        <td>
                            {!! Form::open(['url'=>route('portfolioEdit',['page'=>$portfolio->id]), 'class'=>'form-horizontal', 'method' => 'POST']) !!}
                             {{method_field('DELETE')}}
                             {!! Form::button('<i class="fa fa-remove"></i>',['class'=>'btn btn-danger','type'=>'submit']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>