<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/album.css" rel="stylesheet">
</head>

<body>
<h1>Create Post</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form method="POST" action="/post" accept-charset="UTF-8">
        <input name="_token" type="hidden" value="e7jTeBUGidaibQG5nUCRUPnlSYAjS1U6OZEQ8UQI">
        <div class="form-row">
            <div class="form-group container">
                <label for="name">Name</label>
                <input id="inputName" class="form-control" name="name" type="text">
            </div>
            <div class="form-group container">
                <label for="email">Email</label>
                <input id="inputEmail" class="form-control" name="email" type="email">
            </div>
            <div class="form-group container">
                <label for="site">Site</label>
                <input id="inputSite" class="form-control" name="site" type="text">
            </div>
            <div class="form-group container">
                <label for="message">Message</label>
                <textarea id="inputMessage" class="form-control" name="message" cols="50" rows="10"></textarea>
            </div>
            <div class="form-group container">
                <input class="btn btn-danger" type="submit" value="Send">
            </div>
        </div>
    </form>
    {{--{{ Form::open(['method' => 'post', 'url'=>route('contact')]) }}
        <div class="form-row">
            <div class="form-group container">
                {{Form::label('name', null, ['for'=>'inputName'])}}
                {{Form::text('name', null, ['id'=>'inputName','class' => 'form-control'])}}
            </div>
            <div class="form-group container">
                {{Form::label('email', null, ['for'=>'inputEmail'])}}
                {{Form::email('email', null, ['id'=>'inputEmail','class' => 'form-control'])}}
            </div>
            <div class="form-group container">
                {{Form::label('site', null, ['for'=>'inputSite'])}}
                {{Form::text('site', null, ['id'=>'inputSite','class' => 'form-control'])}}
            </div>
            <div class="form-group container">
                {{Form::label('message', null, ['for'=>'inputMessage'])}}
                {{Form::textarea('message', null, ['id'=>'inputMessage','class' => 'form-control'])}}
            </div>
            <div class="form-group container">
                {{ Form::submit('Send', ['class' => 'btn btn-danger']) }}
            </div>
        </div>
    {{ Form::close() }}--}}
</body>
</html>
