<div class="wrapper container-fluid">
    {!! Form::open(['url'=>route('pagesAdd'), 'class'=>'form-horizontal', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {!! Form::label('name','Page name',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Add text']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('alias','Page alias',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('alias',old('alias'),['class'=>'form-control','placeholder'=>'Add alias']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('alias','Page alias',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {{-- id нужен для визуального редактора --}}
                {!! Form::textarea('text',old('text'),['id'=>'text','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('images','Page images',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {{-- id нужен для визуального редактора --}}
                {!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'btn-primary','data-placeholder'=>'File is absent']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('<i class="fa fa-save"></i> Save',['class'=>'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>