<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PortfolioEditController extends Controller
{
    public function execute(Portfolio $portfolio, Request $request)
    {
        if ($request->isMethod('delete'))
        {
            $portfolio->delete();
            return redirect('admin')->with('status','Portfolio was deleted');
        }

        if ($request->isMethod('post'))
        {
            $input = $request->except('_token');

            $validator = Validator::make($input,
            [
                'name' => 'required|max:255',
                'images' => 'required',
                'filter' => 'required',
            ],
            // переопределяем сообщения об ошибках
            [
                'required'=>'Поле :attribute обязательно к заоплнению',
                'unique'=>'Поле :attribute должно быть уникальным'
            ]);

            if ($validator->fails()){
                return redirect()->route('portfoliosEdit')->withErrors($validator)->withInput();
            }

            if ($request->hasFile('images'))
            {
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();
                $file->move(public_path().'/assets/img/',$input['images']);
            }
            else{
                $input['images'] = $input['old_images'];
            }

            $portfolio->fill($input);

            // если данные добавлены успешно, то редирект на главную админки
            if ($portfolio->update())
            {
                return redirect('admin')->with('status','Portfolio was updated');
            }
        }

        if (view()->exists('admin.portfolios_edit'))
        {
            $data = [
                'title' => 'Редактирование портфолио'.$portfolio->name,
                'data' => $portfolio
            ];
            return view('admin.portfolios_edit',$data);
        }
    }
}
