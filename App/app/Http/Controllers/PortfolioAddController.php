<?php

namespace App\Http\Controllers;

use App\Page;
use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PortfolioAddController extends Controller
{
    public function execute(Request $request)
    {
        // форма отправлена с методом post
        if ($request->isMethod('post'))
        {
            // получаем все поля с формы кроме _token
            $input = $request->except('_token');
            // валидация данных
            $validator = Validator::make($input,
            [
                'name' => 'required|max:255',
                'images' => 'required',
                'filter' => 'required',
            ],
            // переопределяем сообщения об ошибках
            [
                'required'=>'Поле :attribute обязательно к заоплнению',
                'unique'=>'Поле :attribute должно быть уникальным'
            ]);

            if ($validator->fails()){
                return redirect()->route('portfolioAdd')->withErrors($validator)->withInput();
            }

            if ($request->hasFile('images'))
            {
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();
                $file->move(public_path().'/assets/img/',$input['images']);
            }
            else{
                $input['images'] = $input['old_images'];
            }

            // Добавляем данные в модель и сохраняем в таблицу
            /*
            $portfolio = new Portfolio([
                'name' => $input['name'],
                'images' => $input['images'],
                'filter' => $input['filter'],
            ]);
            */
            /*
            $portfolio = new Portfolio();
            $portfolio->name = $input['name'];
            $portfolio->images = $input['name'];
            $portfolio->filter = $input['name'];
            */
            // самый простой способ добавить данные
            // сработает только если имена полей ввода в форме ПОЛНОСТЬЮ совпадут с полями из таблицы
            $portfolio = new Portfolio();
            $portfolio->fill($input);
            // если данные добавлены успешно, то редирект на главную админки
            if ($portfolio->save())
            {
                return redirect('admin')->with('status','Portfolio is added');
            }
        }
        // отображаем форму
        if (view()->exists('admin.portfolios_add'))
        {
            $data = [
                'title' => 'New portfolio'
            ];

            return view('admin.portfolios_add',$data);
        }
    }
}