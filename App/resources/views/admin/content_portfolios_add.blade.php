<div class="wrapper container-fluid">
    {!! Form::open(['url'=>route('portfolioAdd'), 'class'=>'form-horizontal', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {!! Form::label('name','Portfolio name',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Add name']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('filter','Portfolio filter',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('filter',old('filter'),['class'=>'form-control','placeholder'=>'Add filter']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('images','Portfolio images',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {{-- id нужен для визуального редактора --}}
                {!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'btn-primary','data-placeholder'=>'File is absent']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                {!! Form::button('<i class="fa fa-save"></i> Save',['class'=>'btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>