<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests;

class PageController extends Controller
{
    public function execute($alias) {
         if (!$alias){
             abort(404);
         }
         if(view()->exists('site.page')){
             $page = Page::where('alias',$alias)->first();
             //dd($page);
             $data = [
                 'title' => $page->name,
                 'page' => $page
             ];

             return view('site.page',$data);
         }
         else{
             abort(404);
         }
    }
}