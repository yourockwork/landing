<div class="wrapper container-fluid">
    {!! Form::open(['url'=>route('portfolioEdit',['page'=>$data['id']]), 'class'=>'form-horizontal', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::hidden('id',$data['id']) !!}
        {!! Form::label('name','Portfolio name',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('name',$data['name'],['class'=>'form-control','placeholder'=>'Add name']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('filter','Portfolio filter',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('filter',$data['filter'],['class'=>'form-control','placeholder'=>'Add filter']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images','Portfolio images',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Html::image('assets/img/'.$data['images'],'',['class'=>'img-responsive']) !!}
            {!! Form::hidden('old_images',$data['images']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images','Portfolio images',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'btn-primary','data-placeholder'=>'File is absent']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('<i class="fa fa-save"></i> Save',['class'=>'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>