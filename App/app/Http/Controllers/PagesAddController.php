<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PagesAddController extends Controller
{
    public function execute(Request $request)
    {
        if ($request->isMethod('post')){
            $input = $request->except('_token');
            // в результат пихаем только информацию с формы
            // dd($input);
            $messages=[
                'required'=>'Поле :attribute обязательно к заоплнению',
                'unique'=>'Поле :attribute должно быть уникальным'
            ];
            $validator = Validator::make($input,[
                'name' => 'required|max:255',
                'alias' => 'required|unique:pages|max:255',
                'text' => 'required',
                'images' => 'required'
            ],$messages);
            if ($validator->fails()){
                return redirect()->route('pagesAdd')->withErrors($validator)->withInput();
            }
            if ($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();
                $file->move(public_path().'/assets/img',$input['images']);
            }
            $page = new Page();
            // снимает ограничения на редактирование полей в БД
            // $page->unguard();
            // данный прием сработает ТОЛЬКО если имена полей ввода в форме ПОЛНОСТЬЮ совпадут с полями из таблицы
            $page->fill($input);
            if ($page->save())
            {
                return redirect('admin')->with('status','Page is added');
            }
            //dd($input);
        }
        if (view()->exists('admin.pages_add')){
            $data = [
                'title' => 'New Page'
            ];

            return view('admin.pages_add',$data);
        }
    }
}